import { TrackController } from '../src/track/track.controller';
import { TrackService } from '../src/track/track.service';
import { Test } from '@nestjs/testing';
import { Track, TrackDocument } from '../src/track/schemas/track.schema';
import { Comment, CommentDocument } from '../src/track/schemas/comment.schema';
import { FileService } from '../src/file/file.service';
import { Model } from 'mongoose';

describe('trackController', () => {
  let trackController: TrackController;
  let trackService: TrackService;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [TrackController],
      providers: [
        {
          provide: TrackService,
          useValue: {
            getAll: jest.fn(),
          },
        },
      ],
    }).compile();

    trackService = moduleRef.get<TrackService>(TrackService);
    trackController = moduleRef.get<TrackController>(TrackController);
  });

  describe('getAll', () => {
    const fakeComment = new Comment();
    fakeComment.text = 'Text Comment';
    fakeComment.username = 'Username';

    const fakeTrack = new Track();
    fakeTrack.name = 'Test';
    fakeTrack.artist = 'Artist';
    fakeTrack.listens = 10;
    fakeTrack.comments = [fakeComment];

    it('should return an array of cats', async () => {
      const result = [fakeTrack];
      jest.spyOn(trackService, 'getAll').mockResolvedValue(result);
      await expect(await trackController.getAll(10, 0)).toBe(result);
    });
  });
});
